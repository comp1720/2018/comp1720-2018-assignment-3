function preload() {
  // load any assets (images, sounds etc.) here
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  // your persuasive arcade game code goes here
  // but its also a good idea to write some functions
  // which deal with specific parts of your game (like drawing things)
}
